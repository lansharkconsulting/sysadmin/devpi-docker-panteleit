#!/bin/bash

set -e

OFFLINE=${OFFLINE:-}

function log() {
    CYAN='\033[1;36m'
    RESET='\033[0m'
    echo -en "$CYAN"
    echo -n "$@"
    echo -e "$RESET"
}

if [[ ! -e /data/.nodeinfo ]]; then
    log "initializing data directory"
    devpi-server --serverdir /data --init
fi

if [[ $OFFLINE ]]; then
    log "using offline mode"
    OFFLINE="--offline-mode"
fi

log "running server"
exec devpi-server --host=0.0.0.0 --serverdir /data $OFFLINE
